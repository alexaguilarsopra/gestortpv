import React from 'react'
import ReactDOM from 'react-dom'
import Routes from 'routes'

import DrinksContextProvider from './contexts/DrinksContext'
import CartContextProvider from './contexts/CartContext'
import CategoriesContextProvider from './contexts/CategoriesContext'

ReactDOM.render(
  <DrinksContextProvider>
    <CategoriesContextProvider>
      <CartContextProvider>
        <Routes />
      </CartContextProvider>
    </CategoriesContextProvider>
  </DrinksContextProvider>,
  document.getElementById('root')
)
