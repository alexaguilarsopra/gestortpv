import React from 'react'
import Header from 'components/Header'

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <main className="">{children}</main>
      {/* <footer className="mt-5 p-3 footer">2020 &copy; React Store</footer> */}
    </>
  )
}

export default Layout
