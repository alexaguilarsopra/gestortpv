import React, { useState, useContext, useEffect } from 'react'
import { CategoriesContext } from 'contexts/CategoriesContext'
import { Link, useLocation, useRoute } from 'wouter'
import { getSingleDrink } from 'services/getSingleDrink'
import adminServices from 'services/adminServices'
import EditDrinkAssembler from 'helpers/editDrink'
import Modal from 'components/Modal'

const EditDrink = () => {
  const [location, setLocation, queryParams] = useLocation()
  const [, navigate] = useLocation()
  const [match, params] = useRoute('/edit/:id')
  const [singleDrink, setDrink] = useState({})
  const drinkId = params.id
  const [modal, setModal] = useState(false)
  const [title, setTitle] = useState('')

  useEffect(() => {
    getSingleDrink(drinkId).then((drink) => setDrink(drink.drink))
  }, [setDrink, drinkId])

  const modalClose = () => {
    setModal(false)
    navigate('/')
  }

  const modalOpen = () => {
    setModal(true)
  }

  const { _id, brand, category, price, stock, vip } = singleDrink

  let categorie = ''
  let categoryId = ''
  let bottle_price = ''

  if (category) {
    categorie = category.name
  }

  if (category) {
    categoryId = category._id
  }

  if (vip) {
    bottle_price = vip.bottle_price
  }

  const [newBrand, setBrand] = useState(brand)
  const [newBottle_price, setBottlePrice] = useState(bottle_price)
  const [newPrice, setPrice] = useState(price)
  const [newStock, setStock] = useState(stock)
  const [newCategorie, setNewCategorie] = useState(categorie)
  const { categories } = useContext(CategoriesContext)

  const localAssembler = {
    brand,
    bottle_price,
    price,
    stock,
    categoryId,
  }

  const dataToSend = {
    brand: newBrand ? newBrand : brand,
    bottle_price: newBottle_price ? newBottle_price : bottle_price,
    price: newPrice ? newPrice : price,
    stock: newStock ? newStock : stock,
    categoryId: newCategorie ? newCategorie : categoryId,
  }

  const returnedTarget = Object.assign(localAssembler, dataToSend)

  async function editDrink(e) {
    e.preventDefault()
    const data = EditDrinkAssembler.assemble(returnedTarget)
    const response = await adminServices.editDrink(data, _id)
    if (response.message) {
      setTitle('Bebida editada correctamente')
      modalOpen()
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8 m-auto">
          <br />
          <Link to="/" className="btn btn-outline-warning float-left">
            Cancelar
          </Link>
        </div>
        <div className="col-md-8 m-auto">
          <h1 className="display-4 text-center">Editar bebida</h1>
          <form onSubmit={editDrink}>
            <div className="form-group mt-4">
              <label htmlFor="brand">Marca</label>
              <input
                className="form-control"
                type="text"
                placeholder={brand}
                name="brand"
                value={newBrand}
                onChange={(e) => setBrand(e.target.value)}
              />
            </div>
            <div className="form-group mt-4">
              <label htmlFor="price_bottle">Precio Botella</label>
              <input
                className="form-control"
                type="number"
                placeholder={bottle_price}
                name="bottle_price"
                value={newBottle_price}
                onChange={(e) => setBottlePrice(e.target.value)}
              />
            </div>
            <div className="form-group mt-4">
              <label htmlFor="price">Precio Copa</label>
              <input
                className="form-control"
                type="number"
                placeholder={price}
                name="price"
                value={newPrice}
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>
            <div className="form-group mt-4">
              <label htmlFor="stock">Stock</label>
              <input
                className="form-control"
                type="number"
                placeholder={stock}
                name="stock"
                value={newStock}
                onChange={(e) => setStock(e.target.value)}
              />
            </div>
            <div className="form-group mt-4 row">
              <div className="col-8">
                <label htmlFor="categorie">Categoría</label>
                <select
                  name="categorie"
                  className="custom-select form-control"
                  value={newCategorie}
                  onChange={(e) => setNewCategorie(e.target.value)}
                >
                  <option>{categorie}</option>
                  {categories.map((categorie, k) => (
                    <option key={k} value={categorie._id}>
                      {categorie.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <button
              type="submit"
              className="btn btn-outline-warning btn-block mt-4"
            >
              Enviar
            </button>
          </form>
        </div>
      </div>
      <Modal show={modal} handleClose={(e) => modalClose(e)}>
        <div className="container bg-light my-auto mt-5">
          <h2 className="text-center modal-title mt-3">{title}</h2>
          <div className="modal-footer">
            <button className="btn btn-success mt-5" onClick={modalClose}>
              Cerrar
            </button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default EditDrink
