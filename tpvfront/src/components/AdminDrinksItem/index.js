import React, { useState } from 'react'
import { formatNumber } from 'helpers/utils'
import { Link } from 'wouter'
import Modal from 'components/Modal'
import adminServices from 'services/adminServices'

const AdminDrink = ({ drink }) => {
  const [modal, setModal] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')

  const modalClose = () => {
    setModal(false)
  }

  const modalOpen = () => {
    setModal(true)
    setTitle(`¿Deseas borrar la bebida ${drink.brand}? `)
    const deleteButton = (
      <div className="text-center justify-content-around mt-3">
        <button className="btn btn-danger mr-2" onClick={deleteDrink}>
          Borrar
        </button>
        <button className="btn btn-success ml-2" onClick={modalClose}>
          Cerrar
        </button>
      </div>
    )
    setMessage(deleteButton)
  }

  const deleteDrink = (e) => {
    e.preventDefault()
    adminServices.deleteDrink(drink._id)
    modalClose()
    window.history.back()
  }

  return (
    <div className="card card-body m-1">
      <p>{drink.brand}</p>
      <h3 className="text-left">{formatNumber(drink.price)}</h3>
      <div className="text-right">
        <Link
          to={`/edit/${drink._id}`}
          drink={drink}
          className="btn btn-primary btn-sm"
        >
          Editar
        </Link>
        <button className="btn btn-danger btn-sm" onClick={modalOpen}>
          Eliminar
        </button>
      </div>
      <Modal show={modal} handleClose={(e) => modalClose(e)}>
        <div className="container bg-light my-auto mt-5">
          <h2 className="text-center modal-title mt-3">{title}</h2>
          {message}
        </div>
      </Modal>
    </div>
  )
}

export default AdminDrink
