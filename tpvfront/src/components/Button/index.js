import React from 'react'
import './button.scss'

const Button = (props) => {
  const className = `btn button ${props.type}`
  return (
    <button className={className} onClick={props.handleClick} id={props.id}>
      {props.label}
    </button>
  )
}

export default Button
