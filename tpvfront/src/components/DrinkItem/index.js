import React, { useContext } from 'react'
import { CartContext } from 'contexts/CartContext'
import { formatNumber } from 'helpers/utils'

const DrinkItem = ({ drink }) => {
  const { addProduct, cartItems, increase } = useContext(CartContext)

  const isInCart = (drink) => {
    return !!cartItems.find((item) => item._id === drink._id)
  }

  return (
    <div className="card card-body m-1">
      <p>{drink.brand}</p>
      <h3 className="text-left">{formatNumber(drink.price)}</h3>
      <div className="text-right">
        {isInCart(drink) && (
          <button
            onClick={() => increase(drink)}
            className="btn btn-outline-primary btn-sm"
          >
            Añadir más
          </button>
        )}

        {!isInCart(drink) && (
          <button
            onClick={() => addProduct(drink)}
            className="btn btn-primary btn-sm"
          >
            Añadir bebida
          </button>
        )}
      </div>
    </div>
  )
}

export default DrinkItem
