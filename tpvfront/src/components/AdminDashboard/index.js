import React, { useState } from 'react'
import Button from 'components/Button'
import { useLocation } from 'wouter'
import adminServices from 'services/adminServices'
import Modal from 'components/Modal'

const AdminDashboard = () => {
  const [, navigate] = useLocation()
  const [message, setMessage] = useState('')
  const [title, setTitle] = useState('')
  const [modal, setModal] = useState(false)

  const modalClose = () => {
    setModal(false)
  }

  const modalOpen = () => {
    setModal(true)
  }

  const addDrink = () => {
    navigate('/add-drink')
  }

  const addCategory = () => {
    navigate('/add-categorie')
  }

  async function getBottlesConsumed() {
    const bottle_consumed = await adminServices.getBottleConsumedList()

    if (bottle_consumed.count > 0) {
      modalOpen()
      setTitle('Botellas consumidas')
      const drinks = bottle_consumed.drinks.map((drinks) => (
        <ol>
          {drinks.brand} - {drinks.consumed}
        </ol>
      ))
      setMessage(drinks)
    }
  }

  async function getCardBilling() {
    const totalCard = await adminServices.getTotalCardFactured()
    if (totalCard) {
      modalOpen()
      setTitle('Total facturado con Tarjeta')
      setMessage(`${totalCard.total_bill} €`)
    }
  }

  async function getInvitationBilling() {
    const totalInvitation = await adminServices.getTotalInvitationFactured()
    if (totalInvitation) {
      modalOpen()
      setTitle('Total de invitaciones')
      setMessage(`${totalInvitation.total_bill} €`)
    }
  }

  async function getCashBilling() {
    const totalCash = await adminServices.getTotalCashFactured()
    if (totalCash) {
      modalOpen()
      setTitle('Total facturado con Efectivo')
      setMessage(`${totalCash.total_bill} €`)
    }
  }

  const closeBilling = () => {}

  return (
    <>
      <div className="w-100 row mt-3">
        <Button
          type="addDrink"
          handleClick={addDrink}
          label="Añadir bebida"
        />
        <Button
          type="addCategory"
          handleClick={addCategory}
          label="Añadir categoría"
        />
        <Button
          type="getBottlesConsumed"
          handleClick={getBottlesConsumed}
          label="Listado de botellas consumidas"
        />
      </div>
      <div className="w-100 row mt-5">
        <Button
          className="row"
          type="CardBilling"
          handleClick={getCardBilling}
          label="Facturación con tarjeta"
        />
        <Button
          className="row"
          type="CashBilling"
          handleClick={getCashBilling}
          label="Facturación con efectivo"
        />
        <Button
          className="row"
          type="InvitationBilling"
          handleClick={getInvitationBilling}
          label="Total Invitación"
        />
      </div>
      <div className="w-100 row mt-5">
        <Button
          className="row"
          type="CloseBilling"
          handleClick={closeBilling}
          label="Cierre de caja"
        />
      </div>
      <Modal show={modal} handleClose={(e) => modalClose(e)}>
        <div className="container bg-light my-auto mt-5">
          <h2 className="text-center modal-title mt-3 mb-3">{title}</h2>
          <h4 className="text-center">{message}</h4>
          <div className="modal-footer">
            <button className="btn btn-success mt-5" onClick={modalClose}>
              Cerrar
            </button>
          </div>
        </div>
      </Modal>
    </>
  )
}

export default AdminDashboard
