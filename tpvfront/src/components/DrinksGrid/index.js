import React from 'react'
import DrinkItem from 'components/DrinkItem'

const DrinksGrid = ({drinks}) => {
  return (
    <div className="DrinksGrid row">
      {drinks.map((drink) => (
        <div key={drink._id} className="DrinkItem col">
          <DrinkItem drink={drink} />
        </div>
      ))}
    </div>
  )
}

export default DrinksGrid
