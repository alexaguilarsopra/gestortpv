import React from 'react'
import AdminDrinkItem from 'components/AdminDrinksItem'

const AdminDrink = ({drinks}) => {
  return (
    <div className="DrinksGrid row">
      {drinks.map((drink) => (
        <div key={drink._id} className="DrinkItem col">
          <AdminDrinkItem drink={drink} />
        </div>
      ))}
    </div>
  )
}

export default AdminDrink