import React, { useEffect, useState } from 'react'
import { Link } from 'wouter'
import './categories.scss'
import { getCategories } from 'services/getCategories'

const CategoriesGrid = () => {
  const [categories, setCategories] = useState([])

  useEffect(() => {
    getCategories().then((categories) => setCategories(categories.categories))
  }, [setCategories])

  return (
    <div className="categories row">
      <ul>
        {categories && categories.map((categorie) => (
          <li key={categorie._id} className="categorie col">
            <Link href={`/drink/${categorie._id}`}>{categorie.name}</Link>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default CategoriesGrid
