import React, { useContext } from 'react'
import { PlusCircleIcon, MinusCircleIcon, TrashIcon } from 'components/Icons'
import { CartContext } from 'contexts/CartContext'

import { formatNumber } from 'helpers/utils'

const CartItem = ({ product }) => {
  const { increase, decrease, removeProduct } = useContext(CartContext)

  return (
    <div className="d-flex justify-content-around py-2">
      <div className=" p-2">
        <h5 className="mb-1">{product.brand}</h5>
        <p className="mb-1">Price: {formatNumber(product.price)} </p>
      </div>
      <div className=" p-2 text-center ">
        <p className="mb-0"> {product.quantity}</p>
      </div>
      <div className=" p-2 text-right">
        <button
          onClick={() => increase(product)}
          className="btn btn-primary btn-sm mr-2 mb-1"
        >
          <PlusCircleIcon width={'20px'} />
        </button>

        {product.quantity > 1 && (
          <button
            onClick={() => decrease(product)}
            className="btn btn-danger btn-sm mb-1"
          >
            <MinusCircleIcon width={'20px'} />
          </button>
        )}

        {product.quantity === 1 && (
          <button
            onClick={() => removeProduct(product)}
            className="btn btn-danger btn-sm mb-1"
          >
            <TrashIcon width={'20px'} />
          </button>
        )}
      </div>
    </div>
  )
}

export default CartItem
