import React, { useState } from 'react'
import { Link, useLocation } from 'wouter'
import adminServices from 'services/adminServices'
import Modal from 'components/Modal'

const AddDrink = () => {
  const [newCategorie, setName] = useState('')
  const [title, setTitle] = useState('')
  const [modal, setModal] = useState(false)
  const [, navigate] = useLocation()

  const dataToSave = { name: newCategorie }

  const modalClose = () => {
    setModal(false)
    navigate('/')
  }

  const modalOpen = () => {
    setModal(true)
  }

  async function addCategorie(e) {
    e.preventDefault()
    const response = await adminServices.addNewCategorie(dataToSave)
    if (response.message) {
      setTitle('Nueva categoría añadida')
      modalOpen()
    }
    setName('')
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8 m-auto">
          <br />
          <Link to="/" className="btn btn-outline-warning float-left">
            Cancelar
          </Link>
        </div>
        <div className="col-md-8 m-auto">
          <h1 className="display-4 text-center">Añadir Categoría</h1>
          <form onSubmit={addCategorie}>
            <div className="form-group">
              <label htmlFor="Categoria">Añadir Nueva Categoría</label>
              <input
                className="form-control mt-2"
                type="text"
                placeholder="Categoría"
                name="name"
                onChange={(e) => setName(e.target.value)}
                value={newCategorie}
              />
            </div>
            <button
              type="submit"
              className="btn btn-outline-warning btn-block mt-4"
            >
              Enviar
            </button>
          </form>
        </div>
      </div>
      <Modal show={modal} handleClose={(e) => modalClose(e)}>
        <div className="container bg-light my-auto mt-5">
          <h2 className="text-center modal-title mt-3">{title}</h2>
          <div className="modal-footer">
            <button className="btn btn-success mt-5" onClick={modalClose}>
              Cerrar
            </button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default AddDrink
