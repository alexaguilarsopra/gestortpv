import React from 'react'
import './Modal.scss'

const Modal = ({ show, children}) => {
  const showHideClassName = show ? 'modal d-block' : 'modal d-none'

  return (
    <div className={showHideClassName}>
      <div className='modal-container modal-content'>
        {children}
      </div>
    </div>
  )
}

export default Modal