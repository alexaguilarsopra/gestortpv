import React from 'react'
import CategoriesGrid from 'components/Categories'
import styles from './header.module.scss'
import { Link, useLocation } from 'wouter'
import { getToken, removeUserSession } from 'helpers/utils'

const Header = () => {
  const [, navigate] = useLocation()
  const token = getToken()

  const removeToken = () => {
    removeUserSession()
    navigate('/')
    window.location.reload(true)
  }

  return (
    <header className={styles.header}>
      <div className="col-8">
        <CategoriesGrid />
      </div>
      <div className="col-4">
        {token ? (
          <button onClick={removeToken}>Logout</button>
        ) : (
          <Link href="/login">Login</Link>
        )}
      </div>
    </header>
  )
}

export default Header
