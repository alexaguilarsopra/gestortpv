import React, { useState, useEffect } from 'react'
import { Link, useLocation } from 'wouter'
import { getCategories } from 'services/getCategories'
import adminServices from 'services/adminServices'
import AddNewDrink from 'helpers/addNewDrink'
import Modal from 'components/Modal'

const AddDrink = () => {
  const [brand, setBrand] = useState('')
  const [bottle_price, setBottlePrice] = useState('')
  const [price, setPrice] = useState('')
  const [stock, setStock] = useState('')
  const [categorie, setCategorie] = useState('')
  const [categories, setCategories] = useState([])
  const [alcoholic, setAlcoholic] = useState(true)
  const [, navigate] = useLocation()
  const [modal, setModal] = useState(false)
  const [message, setMessage] = useState('')

  const modalClose = () => {
    setModal(false)
    navigate('/')
  }

  const modalOpen = () => {
    setModal(true)
  }

  useEffect(() => {
    getCategories().then((categories) => setCategories(categories.categories))
  }, [setCategories])

  const dataToSave = {
    brand,
    bottle_price,
    price,
    stock,
    categorie,
    alcoholic,
  }

  async function addDrink(e) {
    e.preventDefault()
    const newDrink = AddNewDrink.assemble(dataToSave)
    const response = await adminServices.addNewDrink(newDrink)
    if (response.message) {
      setMessage('Bebida añadida correctamente')
      modalOpen()
    }
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-md-8 m-auto">
          <br />
          <Link to="/" className="btn btn-outline-warning float-left">
            Cancelar
          </Link>
        </div>
        <div className="col-md-8 m-auto">
          <h1 className="display-4 text-center">Añadir bebida</h1>
          <form>
            <div className="form-group mt-4">
              <label className="" htmlFor="brand">
                Marca
              </label>
              <input
                className="form-control"
                type="text"
                placeholder="Marca"
                name="marca"
                value={brand}
                onChange={(e) => setBrand(e.target.value)}
              />
            </div>
            <div className="form-group mt-4">
              <label htmlFor="price_bottle">Precio Botella</label>
              <input
                className="form-control"
                type="number"
                placeholder="Precio Botella"
                name="bottle_price"
                value={bottle_price}
                onChange={(e) => setBottlePrice(e.target.value)}
              />
            </div>
            <div className="form-group mt-4">
              <label htmlFor="price">Precio Copa</label>
              <input
                className="form-control"
                type="number"
                placeholder="Precio Copa"
                name="price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
              />
            </div>
            <div className="form-group mt-4">
              <label htmlFor="stock">Stock</label>
              <input
                className="form-control"
                type="number"
                placeholder="Stock"
                name="stock"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
              />
            </div>
            <div className="form-group mt-4 row">
              <div className="col-8">
                <label htmlFor="categorie">Categoría</label>
                <select
                  name="categorie"
                  className="custom-select form-control"
                  value={categorie}
                  onChange={(e) => setCategorie(e.target.value)}
                >
                  <option>Selecciona una categoría</option>
                  {categories.map((categorie, k) => (
                    <option key={k} value={categorie._id}>
                      {categorie.name}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="form-group form-check mt-4">
              <label htmlFor="alcoholic">No es alcoholica</label>
              <input
                className="form-check-input"
                type="checkbox"
                name="alcholicj"
                onChange={() => setAlcoholic(!alcoholic)}
              />
            </div>
            <button
              type="submit"
              onClick={addDrink}
              className="btn btn-outline-warning btn-block mt-4"
            >
              Enviar
            </button>
          </form>
        </div>
      </div>
      <Modal show={modal} handleClose={(e) => modalClose(e)}>
        <div className="container bg-light my-auto mt-5">
          <h2 className="text-center modal-title mt-3">{message}</h2>
          {/* {message} */}
          <div className="modal-footer">
            <button className="btn btn-success mt-5" onClick={modalClose}>
              Cerrar
            </button>
          </div>
        </div>
      </Modal>
    </div>
  )
}

export default AddDrink
