import React, { useContext, useEffect, useState } from 'react'
import CartProducts from 'components/CartProducts'
import { CartContext } from 'contexts/CartContext'
import { getBillingTypes } from 'services/getBillingTypes'
import { formatNumber } from 'helpers/utils'
import Button from 'components/Button'
import './Cart.scss'

const Cart = () => {
  const {
    total,
    cartItems,
    itemCount,
    clearCart,
    handleCheckout,
    handleBottleConsumed,
  } = useContext(CartContext)

  const [billingTypes, setBillingTypes] = useState([])

  useEffect(() => {
    getBillingTypes().then((billingTypes) =>
      setBillingTypes(billingTypes.billingTypes)
    )
  }, [])

  return (
    <>
      <div className="justify-content-center w-100">
        <div className="display overflow-auto row h-50 w-100">
          {cartItems.length > 0 ? (
            <CartProducts />
          ) : (
            <div className="d-flex justify-content-around py-2">
              Carrito vacio
            </div>
          )}
        </div>
        <div className="total-cart row text-center">
          {/* {cartItems.length > 0 && ( */}
            <div className="d-flex mx-auto">
              <p className="item-count-text mb-1">Total Items</p>
              <h4 className="item-count mb-3 txt-right">{itemCount}</h4>
              <p className="total-amount-text mb-1">Total a pagar</p>
              <h4 className="total-amount  txt-right">{formatNumber(total)}</h4>
            </div>
          {/* )} */}
        </div>
        <div className="checkout-buttons justify-content-center text-center row h-25">
          {billingTypes.length > 0 &&
            billingTypes.map((billingType) => (
              <Button
                type={billingType.type}
                id={billingType._id}
                handleClick={handleCheckout}
                label={billingType.type}
              />
            ))}
          <Button
            type="consumida"
            handleClick={handleBottleConsumed}
            label="BOTELLA CONSUMIDA"
          />
          <Button type="clear" handleClick={clearCart} label="BORRAR TODO" />
        </div>
      </div>
    </>
  )
}

export default Cart
