import React, { useEffect, useState } from 'react'
import Layout from 'components/Layout'
import DrinksGrid from 'components/DrinksGrid'
import Cart from 'components/Cart'
import AdminDrink from 'components/AdminDrinks'
import AdminDashboard from 'components/AdminDashboard'
import { getToken } from 'helpers/utils'
import { getDrinks } from 'services/getDrinks'

export default function Home() {
  const [drinks, setDrinks] = useState([])
  const token = getToken()

  useEffect(() => {
    getDrinks().then((drinks) => setDrinks(drinks.drinks))
  }, [setDrinks])

  const bestOf = drinks.filter(drink => drink.best_of === true)

  return (
    <>
      <Layout>
        <div className="row">
          <div className="col-8">
            <pre>{drinks.brand}</pre>
            {!token ? (
              <DrinksGrid drinks={bestOf} />
            ) : (
              <AdminDrink drinks={bestOf} />
            )}
          </div>
          <div className="col-4">{token ? <AdminDashboard /> : <Cart />}</div>
        </div>
      </Layout>
    </>
  )
}
