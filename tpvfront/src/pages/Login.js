import React, { useState } from 'react'
import { Link } from 'wouter'
import axios from 'axios'
import { setUserSession } from 'helpers/utils'
import {useLocation} from 'wouter'

function Login(props) {
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [password, setPass] = useState('')
  const [error, setError] = useState(null)
  const [, navigate] = useLocation()

  const handleLogin = () => {
    setError(null)
    setLoading(true)
    axios.post('http://localhost:9000/user/login', { email: email, password: password }).then(response => {
      setLoading(false)
      setUserSession(response.data.token, response.data.payload.user.email)
      navigate('/')
    }).catch(error => {
      setLoading(false)
      console.log(error)
      if (error.response.status === 401) setError(error.response.data.message)
      if (error.response.status === 400) setError(error.response.data.message)
      else setError('El usuario no existe')
    })
  }

  return (
      <div className='container'>
        <div className='col-md-8 m-auto'>
          <br />
          <Link to='/' className='btn btn-outline-warning float-left'>Atrás</Link>
        </div>
        <h3 className='text-center'>Login</h3>
        <div className='text-center mt-3'>
          {error && <><small style={{color: 'red'}}>{error}</small></>}<br/>
        </div>
        <div className='form-group text-center mt-3'>
          <label>Usename</label> 
          <input className='form-control w-50 mx-auto mt-2' type='text' name='email' value={email} onChange={e => setEmail(e.target.value)} />
        </div>
        <div className='form-group text-center mt-3'>
          <label>Password</label>
          <input className='form-control w-50 mx-auto mt-2' type='password' name='password' value={password} onChange={e => setPass(e.target.value)}  />
        </div>
        <div className='text-center mt-5'>
          <button className='btn btn-primary w-50' type='button' value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading}>Enviar</button>
        </div>
      </div>
  )
}

export default Login
