import React, { useEffect, useContext } from 'react'
import { getDrinksByCategory } from 'services/getDrinksByCategory'
import Layout from 'components/Layout'
import DrinksGrid from 'components/DrinksGrid'
import Cart from 'components/Cart'
import AdminDashboard from 'components/AdminDashboard'
import AdminDrink from 'components/AdminDrinks'
import { DrinksContext } from 'contexts/DrinksContext'
import { getToken } from 'helpers/utils'

export default function DrinksByCategory({ params }) {
  const { drinks, setDrinks } = useContext(DrinksContext)
  const { categoryId } = params

  useEffect(() => {
    getDrinksByCategory({ categoryId }).then((drinks) =>
      setDrinks(drinks.drinks)
    )
  }, [setDrinks, categoryId])

  const token = getToken()

  return (
    <>
      <Layout>
        <div className="row">
          <div className="col-8">
            {!token ? (
              <DrinksGrid drinks={drinks} />
            ) : (
              <AdminDrink drinks={drinks} />
            )}
          </div>
          <div className="col-4">
                {token ? <AdminDashboard /> : <Cart />}
          </div>
        </div>
      </Layout>
    </>
  )
}
