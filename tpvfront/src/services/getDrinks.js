export const getDrinks = () => {
  const apiURL = 'http://localhost:9000/drinks'

  return fetch(apiURL).then((res) => res.json())
}
