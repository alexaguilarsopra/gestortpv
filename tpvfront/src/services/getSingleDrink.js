export const getSingleDrink = (id) => {
  const apiURL = `http://localhost:9000/drinks/${id}`

  return fetch(apiURL).then((res) => res.json())
}
