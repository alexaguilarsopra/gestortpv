export const getBillingTypes = () => {
  const apiURL = 'http://localhost:9000/billingTypes'

  return fetch(apiURL).then((res) => res.json())
}
