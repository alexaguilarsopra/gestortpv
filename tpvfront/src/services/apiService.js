/* eslint-disable import/no-anonymous-default-export */
import axios from 'axios'

export default {
  async get(url) {
    const response = await axios.get(url)

    return response.data
  },
  async post(url, data = {}) {
    const response = await axios.post(url, data)

    return response.data
  },
  async patch(url, data = {}) {
    const response = await axios.patch(url, data)

    return response.data
  },

  async delete(url) {
    const response = await axios.delete(url)

    return response
  },
}
