export const getDrinksByCategory = ({ categoryId }) => {
  const apiUrl = `http://localhost:9000/drinks/drink-category/${categoryId}`

  return fetch(apiUrl).then((res) => res.json())
}
