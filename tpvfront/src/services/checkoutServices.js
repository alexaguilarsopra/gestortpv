import api from './apiService'

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  checkoutService(state) {
    return api.post('http://localhost:9000/orders', state)
  },

  async createBill(data) {
    const bill = await api.post('http://localhost:9000/bills', data)
    return bill
  },

  addBottleConsumed(state) {
    return api.patch('http://localhost:9000/drinks/bottle-consumed', state)
  }
}
