import api from './apiService'

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  addNewDrink(data) {
    return api.post('http://localhost:9000/drinks', data)
  },

  editDrink(data, drinkId) {
    return api.patch(`http://localhost:9000/drinks/${drinkId}`, data)
  },

  addNewCategorie(data) {
    return api.post('http://localhost:9000/categories', data)
  },

  getBottleConsumedList() {
    return api.get('http://localhost:9000/drinks/bottle-consumed')
  },

  getTotalCashFactured() {
    return api.get('http://localhost:9000/bills/5fa291652f6fe6508317caab')
  },

  getTotalCardFactured() {
    return api.get('http://localhost:9000/bills/5fa28d9da7acf14f13180d1d')
  },

  getTotalInvitationFactured() {
    return api.get('http://localhost:9000/bills/5fa28da9a7acf14f13180d1e')
  },

  deleteDrink(drinkId) {
    return api.delete(`http://localhost:9000/drinks/${drinkId}`)
  }
}
