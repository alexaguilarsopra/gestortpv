export const getCategories = () => {
  const apiURL = 'http://localhost:9000/categories'

  return fetch(apiURL).then((res) => res.json())
}
