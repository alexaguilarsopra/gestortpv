import React, { createContext, useState, useEffect } from 'react'
import { getCategories } from 'services/getCategories'
export const CategoriesContext = createContext()

const CategoriesContextProvider = ({ children }) => {
  const [categories, setCategories] = useState([])

  useEffect(() => {
    getCategories().then((categories) => setCategories(categories.categories))
  }, [setCategories])

  return (
    <CategoriesContext.Provider value={{ categories, setCategories }}>
      {children}
    </CategoriesContext.Provider>
  )
}

export default CategoriesContextProvider
