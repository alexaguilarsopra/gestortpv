import React, { createContext, useReducer } from 'react'
import { CartReducer, sumItems } from './CartReducer'

export const CartContext = createContext()

const storage = localStorage.getItem('cart')
  ? JSON.parse(localStorage.getItem('cart'))
  : []
const initialState = {
  cartItems: storage,
  ...sumItems(storage),
  checkout: false,
  checkoutType: '',
  status: false,
}

const CartContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(CartReducer, initialState)

  const increase = (payload) => {
    dispatch({ type: 'INCREASE', payload })
  }

  const decrease = (payload) => {
    dispatch({ type: 'DECREASE', payload })
  }

  const addProduct = (payload) => {
    dispatch({ type: 'ADD_ITEM', payload })
  }

  const removeProduct = (payload) => {
    dispatch({ type: 'REMOVE_ITEM', payload })
  }

  const clearCart = () => {
    dispatch({ type: 'CLEAR' })
  }

  const handleCheckout = (id) => {
    let checkoutType = id.target.id
    dispatch({type: 'CHECKOUT', state: {state, checkoutType}})
  }

  const handleBottleConsumed = () => {
    dispatch({ type: 'BOTTLE_CONSUMED'})
  }

  const contextValues = {
    removeProduct,
    addProduct,
    increase,
    decrease,
    clearCart,
    handleCheckout,
    handleBottleConsumed,
    ...state,
  }

  return (
    <CartContext.Provider value={contextValues}>
      {children}
    </CartContext.Provider>
  )
}

export default CartContextProvider
