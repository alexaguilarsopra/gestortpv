import checkoutServices from 'services/checkoutServices'
import OrderAssembler from 'helpers/orderAssembler'
import BottleConsumedAssembler from 'helpers/bottleConsumedAssembler'

const Storage = (cartItems) => {
  localStorage.setItem(
    'cart',
    JSON.stringify(cartItems.length > 0 ? cartItems : [])
  )
}

export const sumItems = (cartItems) => {
  Storage(cartItems)
  let itemCount = cartItems.reduce(
    (total, product) => total + product.quantity,
    0
  )
  let total = cartItems
    .reduce((total, product) => total + product.price * product.quantity, 0)
    .toFixed(2)
  return { itemCount, total }
}

export const CartReducer = (state, action) => {
  switch (action.type) {
    case 'ADD_ITEM':
      if (!state.cartItems.find((item) => item._id === action.payload._id)) {
        state.cartItems.push({
          ...action.payload,
          quantity: 1,
        })
      }

      return {
        ...state,
        ...sumItems(state.cartItems),
        cartItems: [...state.cartItems],
      }
    case 'REMOVE_ITEM':
      return {
        ...state,
        ...sumItems(
          state.cartItems.filter((item) => item._id !== action.payload._id)
        ),
        cartItems: [
          ...state.cartItems.filter((item) => item._id !== action.payload._id),
        ],
      }
    case 'INCREASE':
      state.cartItems[
        state.cartItems.findIndex((item) => item._id === action.payload._id)
      ].quantity++
      return {
        ...state,
        ...sumItems(state.cartItems),
        cartItems: [...state.cartItems],
      }
    case 'DECREASE':
      state.cartItems[
        state.cartItems.findIndex((item) => item._id === action.payload._id)
      ].quantity--
      return {
        ...state,
        ...sumItems(state.cartItems),
        cartItems: [...state.cartItems],
      }
    case 'CHECKOUT':
      const billingTypeId = action.state.checkoutType
      const dataToSave = OrderAssembler.assemble(state, billingTypeId)
      checkoutServices.checkoutService(dataToSave).then((res) => {
        const orderId = res.createdOrder._id
        const dataForBill = { billingTypeId, orderId }
        const billCreated = checkoutServices
          .createBill(dataForBill)
          .then((res) => {
            return res
          })
        return billCreated
      })
      return {
        cartItems: [],
        checkout: true,
        ...sumItems([]),
      }
    case 'BOTTLE_CONSUMED':
      const bottleConsumedToSave = BottleConsumedAssembler.assemble(state)
      checkoutServices.addBottleConsumed(bottleConsumedToSave)
      return {
        cartItems: [],
        checkout: true,
        ...sumItems([]),
      }
    case 'CLEAR':
      return {
        cartItems: [],
        ...sumItems([]),
      }
    default:
      return state
  }
}
