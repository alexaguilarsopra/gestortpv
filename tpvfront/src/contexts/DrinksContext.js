import React, { createContext, useState, useEffect } from 'react'
import { getDrinks } from 'services/getDrinks'
export const DrinksContext = createContext()

const DrinksContextProvider = ({ children }) => {
  const [drinks, setDrinks] = useState([])

  useEffect(() => {
    getDrinks().then((drinks) => setDrinks(drinks.drinks))
  }, [setDrinks])

  return (
    <DrinksContext.Provider value={{ drinks, setDrinks }}>
      {children}
    </DrinksContext.Provider>
  )
}

export default DrinksContextProvider
