import React from 'react'
import { Router, Switch, Route } from 'wouter'

import Home from 'pages/Home'
import DrinksByCategory from 'pages/DrinksByCategory'
import Login from 'pages/Login'
import AddDrinkPage from 'pages/AddDrinkPage'
import AddCategoriePage from 'pages/AddCategoriePage'
import EditDrinkPage from 'pages/EditDrinkPage'

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" component={Home} />
        <Route path="/drink/:categoryId" component={DrinksByCategory} />
        <Route path="/login" component={Login} />
        <Route path="/add-drink" component={AddDrinkPage} />
        <Route path="/add-categorie" component={AddCategoriePage} />
        <Route path="/edit/:id" component={EditDrinkPage} />
      </Switch>
    </Router>
  )
}

export default Routes
