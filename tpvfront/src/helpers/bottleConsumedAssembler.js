class BottleConsumedAssembler {
  assemble(data) {
    if (!data) return

    const items = data.cartItems

    return items.map(item => {
      return {
        drinkId: item._id,
        quantity: item.quantity
      }
    })
  }

  assembleDrink(items) {
    if (!items) return 

    return items.map(item => {
      return {
        drinkId: item._id,
        quantity: item.quantity
      }
    })
  }
}

export default new BottleConsumedAssembler()