class AddNewDrink {
  assemble(data) {
    if (!data) return

    return {
      brand: data.brand,
      price: parseInt(data.price),
      stock: parseInt(data.stock),
      consumed: 0,
      categoryId: data.categorie,
      alcoholic: data.alcoholic,
      best_of: false,
      bottle_price: parseInt(data.bottle_price),
    }
  }
}

export default new AddNewDrink()
