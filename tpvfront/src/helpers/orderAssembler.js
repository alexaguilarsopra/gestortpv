class OrderAssembler {
  assemble(data, checkoutType) {
    if (!data || !checkoutType) return

    const drinks = data.cartItems
    
    const order = drinks.map(drink => {
      return {
        drinkId: drink._id,
        quantity: drink.quantity,
        charge: drink.quantity * drink.price
      }
    })

    return order
  }
}

export default new OrderAssembler()
