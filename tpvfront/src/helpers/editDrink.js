class EditDrinkAssembler {
  assemble(data) {
    if (!data) return

    return {
      brand: data.brand,
      price: parseInt(data.price),
      stock: parseInt(data.stock),
      categoryId: data.categoryId,
      bottle_price: parseInt(data.bottle_price),
    }
  }
}

export default new EditDrinkAssembler()
