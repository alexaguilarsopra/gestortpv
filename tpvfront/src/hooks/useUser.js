import { useContext, useCallback, useState } from 'react'
import Context from 'contexts/UserContext'
import loginService from 'services/login'

export default function useUser() {
  const {jwt, setJWT} = useContext(Context)
  const [state, setState] = useState({ loading: false, error: false})

  const login = useCallback(({ email, password }) => {
    setState({loading: true, error: false})
    loginService({email, password})
      .then(jwt => {
        window.sessionStorage.setItem('jwt', jwt)
        setState({loading: false, error: false})
        setJWT(jwt)
      })
      .catch(err => {
        window.sessionStorage.removeItem('jwt', jwt)
        setState({loading: false, error: true})
        console.log(err)
      })
  }, [jwt, setJWT])

  const logout = useCallback(() => {
    window.sessionStorage.removeItem('jwt', jwt)
    setJWT(null)
  }, [jwt, setJWT])

  return {
    isLogged: Boolean(jwt),
    isLogingLoading: state.loading,
    hasLoginError: state.error,
    login,
    logout
  }
}