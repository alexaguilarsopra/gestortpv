const mongoose = require('mongoose')
const BillingType = require('../models/billingType')

exports.get_all_billing_types = (req, res) => {
  BillingType.find()
    .select('_id type')
    .then((billingTypes) => {
      const response = {
        count: billingTypes.length,
        billingTypes: billingTypes.map((billingType) => {
          return {
            type: billingType.type,
            _id: billingType._id,
          }
        }),
      }
      res.status(200).json(response)
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({ error: err })
    })
}

exports.create_billing_type = (req, res) => {
  const billingType = new BillingType({
    _id: new mongoose.Types.ObjectId(),
    type: req.body.type,
  })
  billingType
    .save()
    .then((result) => {
      res.status(200).json({
        message: 'Created billing type successfully',
        createdType: {
          type: result.type,
          _id: result._id,
        },
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({ error: err })
    })
}

exports.get_single_billing_type = (req, res) => {
  BillingType.findById(req.params.typeId)
    .then((billingType) => {
      if (!billingType)
        res.status(404).json({ message: 'Billing type not found' })

      res.status(200).json({
        billingType,
      })
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      })
    })
}

exports.update_billing_type = (req, res) => {
  const id = req.params.typeId
  const newType = req.body.type
  
  BillingType.update({ _id: id }, { $set: { type: newType } })
    .then(() => {
      res.status(200).json({
        message: 'Billing type updated',
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({
        error: err,
      })
    })
}

exports.delete_billing_type = (req, res) => {
  BillingType.deleteOne({ _id: req.params.typeId })
    .then(() => {
      res.status(200).json({
        message: 'Billing type deleted',
      })
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      })
    })
}
