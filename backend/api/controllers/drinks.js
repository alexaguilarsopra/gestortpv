const mongoose = require('mongoose')

const Drink = require('../models/drink')
const Category = require('../models/category')

exports.get_all_drinks = (req, res) => {
  Drink.find()
    .where('stock')
    .gt(0)
    .select('brand price stock consumed category alcoholic best_of vip')
    .populate('category', 'name')
    .then((drinks) => {
      res.status(200).json({
        count: drinks.length,
        drinks: drinks.map((drink) => {
          return {
            _id: drink._id,
            brand: drink.brand,
            price: drink.price,
            stock: drink.stock,
            consumed: drink.consumed,
            category: drink.category,
            alcoholic: drink.alcoholic,
            best_of: drink.best_of,
            vip: drink.vip,
          }
        }),
      })
    })
    .catch((err) => {
      res.status(500).json({ error: err })
    })
}

exports.create_drink = (req, res) => {
  Category.findById(req.body.categoryId)
    .then((category) => {
      if (!category) res.status(404).json({ message: 'Category not found' })

      const drink = new Drink({
        _id: mongoose.Types.ObjectId(),
        brand: req.body.brand,
        price: req.body.price,
        stock: req.body.stock,
        consumed: req.body.consumed,
        category: req.body.categoryId,
        alcoholic: req.body.alcoholic,
        best_of: req.body.best_of,
        vip: {
          bottle_price: req.body.bottle_price,
        },
      })
      return drink.save()
    })
    .then((result) => {
      res.status(201).json({
        message: 'Drink created',
        createdDrink: {
          _id: result._id,
          brand: result.brand,
          price: result.price,
          stock: result.stock,
          consumed: result.consumed,
          category: result.category,
          alcoholic: result.alcoholic,
          best_of: result.best_of,
          vip: result.vip,
        },
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({
        error: err,
      })
    })
}

exports.get_single_drink = (req, res) => {
  Drink.findById(req.params.drinkId)
    .populate('category')
    .then((drink) => {
      if (!drink)
        return res.status(404).json({
          message: 'Drink not found',
        })
      res.status(200).json({
        drink,
      })
    })
    .catch((err) => {
      res.status(500).json({ error: err })
    })
}

exports.get_drink_by_category = (req, res) => {
  Drink.find({ category: req.params.categoryId })
    .where('stock')
    .gt(0)
    .populate('category')
    .then((drinks) => {
      if (!drinks)
        return res.status(404).json({
          message: 'Category not found',
        })
      res.status(200).json({
        count: drinks.length,
        drinks: drinks.map((drink) => {
          return {
            _id: drink._id,
            brand: drink.brand,
            price: drink.price,
            stock: drink.stock,
            consumed: drink.consumed,
            category: drink.category,
            alcoholic: drink.alcoholic,
            best_of: drink.best_of,
            vip: drink.vip,
          }
        }),
      })
    })
}

exports.get_consumed_bottles = (req, res) => {
  Drink.find()
    .where('consumed')
    .gt(0)
    .select('brand consumed')
    .then((drinks) => {
      res.status(200).json({
        count: drinks.length,
        drinks: drinks.map((drink) => {
          return {
            _id: drink._id,
            brand: drink.brand,
            price: drink.price,
            stock: drink.stock,
            consumed: drink.consumed,
            category: drink.category,
            alcoholic: drink.alcoholic,
            best_of: drink.best_of,
            vip: drink.vip,
          }
        }),
      })
    })
    .catch((err) => {
      res.status(500).json({ error: err })
    })
}

exports.update_stock_and_consumed = (req, res) => {
  const drinks = req.body
  drinks.map((drink) => {
    Drink.findById(drink.drinkId)
      .then((indiDrink) => {
        if (!indiDrink) res.status(404).json({ message: 'IndiDrink not found' })

        const id = indiDrink._id
        const stock = indiDrink.stock
        const consumed = indiDrink.consumed
        const newStock = stock - drink.quantity
        const newConsumed = consumed + drink.quantity

        return Drink.updateOne(
          { _id: id },
          { $set: { stock: newStock, consumed: newConsumed } }
        )
      })
      .then(() => {
        res.status(200).json({
          message: 'Consumed updated',
        })
      })
      .catch((err) => {
        res.status(500).json({
          status: 'error',
          error: err,
        })
      })
  })
}

exports.update_drink = (req, res) => {
  const id = req.params.drinkId
  const updateOps = {
    brand: req.body.brand,
    price: req.body.price,
    bottle_price: req.body.bottle_price,
    category: req.body.categoryId,
    stock: req.body.stock,
  }
  Drink.update({ _id: id }, { $set: updateOps })
    .then(() => {
      res.status(200).json({
        message: 'Category updated',
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({
        error: err,
      })
    })
}

exports.delete_drink = (req, res) => {
  Drink.remove({ _id: req.params.drinkId })
    .then(() => {
      res.status(200).json({
        message: 'Drink deleted',
      })
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      })
    })
}
