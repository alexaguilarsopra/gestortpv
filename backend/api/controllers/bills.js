const mongoose = require('mongoose')

const Bill = require('../models/billing')
const BillingType = require('../models/billingType')
const Order = require('../models/order')

exports.get_all_bills = (req, res) => {
  Bill.find()
    .select('billingType order total')
    .populate(['billingType', 'order'])
    .then((bills) => {
      res.status(200).json({
        count: bills.length,
        bills: bills.map((billing) => {
          return {
            _id: billing._id,
            billingType: billing.billingType.type,
            total: billing.total,
          }
        }),
      })
    })
    .catch((err) => {
      res.status(500).json({ error: err })
    })
}

exports.create_bill = (req, res) => {
  Order.findById(req.body.orderId).then((order) => {
    const drinks = order.drinks
    let total = 0
    drinks.map((drink) => {
      const charge = drink.charge
      total += charge

      return total
    })
    if (!order) res.status(200).json({ message: 'Order not found' })
    BillingType.findById(req.body.billingTypeId)
      .then((billingType) => {
        if (!billingType) res.status('Billing type not found')
        const bill = new Bill({
          _id: mongoose.Types.ObjectId(),
          billingType: req.body.billingTypeId,
          order: req.body.orderId,
          total: total,
        })
        return bill.save()
      })
      .then((result) => {
        res.status(201).json({
          message: 'Bill created',
          status: 'OK',
          billCreated: {
            _id: result._id,
            billingType: result.billingType,
            total: result.total,
          },
        })
      })
      .catch((err) => {
        console.log(err)
        res.status(500).json({
          error: err,
        })
      })
  })
}

exports.get_bills_by_type = (req, res) => {
  Bill.find({ billingType: req.params.billingTypeId })
    .select('billingType order total')
    .populate(['billingType', 'order'])
    .then((bills) => {
      let total_bill = 0
      bills.map((bill) => {
        total_bill += bill.total
        return total_bill
      })
    res.status(200).json({total_bill})
  })
}

exports.get_total_bills = (req, res) => {
  Bill.find()
    .select('total')
    .populate({ path: 'total', select: 'charge' })
    .then((bills) => {
      let totalBill = 0
      bills.map((bill) => {
        totalBill += bill.total.charge
      })
      res.status(200).json({ totalBill })
    })
}
