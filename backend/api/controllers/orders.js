const mongoose = require('mongoose')

const Order = require('../models/order')
const Drink = require('../models/drink')
const order = require('../models/order')

exports.get_all_orders = (req, res) => {
  Order.find()
    .select('drinks quantity charge date')
    .populate('drinks')
    .then((orders) => {
      res.status(200).json({
        count: orders.length,
        orders: orders.map((order) => {
          return {
            _id: order._id,
            drinks: order.drinks,
            date: order.date,
          }
        }),
      })
    })
    .catch((err) => {
      res.status(500).json({ error: err })
    })
}

exports.add_order = (req, res) => {
  const orders = req.body
  const newOrder = new Order({
    _id: new mongoose.Types.ObjectId(),
    drinks: orders,
    date: new Date(),
  })

  newOrder
    .save()
    .then((result) => {
      res.status(200).json({
        message: 'Order created',
        createdOrder: {
          _id: result._id,
          drinks: result.drinks,
          date: result.date,
        },
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({ error: err })
    })
}

exports.delete_order = (req, res) => {
  Order.deleteOne({ _id: req.params.orderId })
    .then(() => {
      res.status(200).json({
        message: 'Order removed',
      })
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      })
    })
}
