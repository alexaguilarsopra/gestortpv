const mongoose = require('mongoose')
const Category = require('../models/category')

exports.get_all_categories = (req, res) => {
  Category.find()
    .select('name _id')
    .then((categories) => {
      const response = {
        count: categories.length,
        categories: categories.map((category) => {
          return {
            name: category.name,
            _id: category._id,
          }
        }),
      }
      res.status(200).json(response)
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({ error: err })
    })
}

exports.create_category = (req, res) => {
  const category = new Category({
    _id: new mongoose.Types.ObjectId(),
    name: req.body.name,
  })
  category
    .save()
    .then((result) => {
      res.status(201).json({
        message: 'Created category successfully',
        createdCategory: {
          name: result.name,
          _id: result._id,
        },
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({ error: err })
    })
}

exports.get_single_category = (req, res) => {
  Category.findById(req.params.categoryId)
    .then((category) => {
      if (!category) {
        return res.status(404).json({
          message: 'Category not found',
        })
      }
      res.status(200).json({
        category,
      })
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      })
    })
}

exports.update_category = (req, res) => {
  const id = req.params.categoryId
  const updateOps = {}
  for (const ops of req.body) {
    if (ops.value === '' || ops.value === null)
      return res.status(404).json({ message: 'Algo mal' })
    updateOps[ops.propName] = ops.value
  }
  Category.update({ _id: id }, { $set: updateOps })
    .then(() => {
      res.status(200).json({
        message: 'Category updated',
      })
    })
    .catch((err) => {
      console.log(err)
      res.status(500).json({
        error: err,
      })
    })
}

exports.delete_category = (req, res) => {
  Category.remove({ _id: req.params.categoryId })
    .then(() => {
      res.status(200).json({
        message: 'Category deleted',
      })
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
      })
    })
}
