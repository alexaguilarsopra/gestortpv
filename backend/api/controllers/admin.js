const Admin = require('../models/admin');
const jwt = require('jsonwebtoken')

exports.getAdmin = (req, res) => {
    Admin.find()
        .then(admin => {
            res.send(admin);
        }).catch(err => {
            res.status(500).send('Error al obtener admin');
        })
}

exports.crearAdmin = (req, res) => {
    if (!req.body) res.status(400).send({ message: 'El contenido no puede estar vacio' })

    const newAdmin = new Admin({
        password: req.body.password
    })

    newAdmin.save()
        .then(data => {
            res.send(data)
        }).catch(err => {
            res.status(500).send({ message: err.message || 'Error al crear admin' })
        })
}

exports.authenticate = (req, res) => {
    const {email, password} = req.body
    Admin.findOne({email}, (err, user) => {
        if (err) {
            console.log(err)
            res.status(500).json({ error: 'Internal error please try again'})
        } else if (!user) {
            res.status(401).json({ error: 'Incorrect username o pass'})
        } else {
            user.isCorrectPassword(password, (err, same) => {
                if (err) {
                    res.status(500).json({ error: 'Internal error please try again'})
                } else if (!same) {
                    res.status(401).json({ error: 'Incorrect username or password' })
                } else {
                    const payload = { email };
                    const token = jwt.sign(payload, 'secret', {expiresIn: '1h'})
                    console.log('token', token)
                    res.cookie('token', token, { httpOnly: true }).sendStatus(200)
                }
            })
        }
    })
}