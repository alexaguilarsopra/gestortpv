const express = require('express')
const router = express.Router()

const BillingTypesController = require('../controllers/billingTypes')

router.get('/', BillingTypesController.get_all_billing_types)

router.post('/', BillingTypesController.create_billing_type)

router.get('/:typeId', BillingTypesController.get_single_billing_type)

router.patch('/:typeId', BillingTypesController.update_billing_type)

router.delete('/:typeId', BillingTypesController.delete_billing_type)

module.exports = router