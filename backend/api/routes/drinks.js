const express = require('express')
const router = express.Router()

const DrinksController = require('../controllers/drinks')

router.get('/', DrinksController.get_all_drinks)

router.get('/bottle-consumed', DrinksController.get_consumed_bottles)

router.post('/', DrinksController.create_drink)

router.get('/:drinkId', DrinksController.get_single_drink)

router.get('/drink-category/:categoryId', DrinksController.get_drink_by_category)

router.patch('/bottle-consumed', DrinksController.update_stock_and_consumed)

router.patch('/:drinkId', DrinksController.update_drink)

router.delete('/:drinkId', DrinksController.delete_drink)

module.exports = router
