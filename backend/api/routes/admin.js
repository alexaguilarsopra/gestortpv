const express = require('express')
const { check, validationResult } = require('express-validator')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const router = express.Router()
const adminController = require('../controllers/admin')

router.post('/', adminController.authenticate)
const auth = require('../middleware/auth')

const User = require('../models/admin')

router.post('/login', [
  check('email', 'Please enter a valid email').isLength(),
  check('password', 'Please enter a valid password').isLength()
],
 async (req, res) => {
  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return res.status(400).json({
      errors: errors.array()
    })
  }

  const { email, password } = req.body
  try {
    const user = await User.findOne({
      email
    })

    const compB = bcrypt.compareSync(password, user.password)

    const salt = 4
    const passwordCod = bcrypt.hashSync(password, salt)

    if (!user) return res.status(400).json({ message: 'El usuario no existe' })
    if (!compB) return res.status(400).json({ message: 'La contraseña no es valida' })

    const payload = { user: 
      { 
        id: user.id,
        email: user.email,
      }
    }
    
    jwt.sign(payload, 'secret', { expiresIn: 3600 }, (err, token) => {
      if (err) throw err
      res.status(200).json({ token, payload })
    })
  } catch (e) {
    res.status(500).json({ message: 'Server Error' })
  }
})

router.get('/me', auth, async (req, res) => {
  try {
    //request.user is getting fetched from Middleware after token authentication
    const user = await User.findById(req.user.id)
    res.json(user)
  } catch (e) {
    res.send({ message: 'Error in Fetching user' })
  }
})

module.exports = router