const express = require('express')
const router = express.Router()

const BillingsController = require('../controllers/bills')

router.get('/', BillingsController.get_all_bills)

router.get('/total', BillingsController.get_total_bills)

router.post('/', BillingsController.create_bill)

router.get('/:billingTypeId', BillingsController.get_bills_by_type)

module.exports = router