const express = require('express')
const router = express.Router()

const OrdersControllers = require('../controllers/orders')

router.get('/', OrdersControllers.get_all_orders)

router.post('/', OrdersControllers.add_order)

router.delete('/:orderId', OrdersControllers.delete_order)

module.exports = router