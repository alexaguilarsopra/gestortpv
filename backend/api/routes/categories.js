const express = require('express')
const router = express.Router()

const CategoriesController = require('../controllers/categories')

router.get('/', CategoriesController.get_all_categories)

router.post('/', CategoriesController.create_category)

router.get('/:categoryId', CategoriesController.get_single_category)

router.patch('/:categoryId', CategoriesController.update_category)

router.delete('/:caegoryId', CategoriesController.delete_category)

module.exports = router
