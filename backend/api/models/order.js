const mongoose = require('mongoose')

const orderSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  drinks: [{
    drinkId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Drink',
      required: true,
    },
    quantity: { type: Number, required: true },
    charge: { type: Number, required: true },
  }],
  date: { type: Date, required: true },
})

module.exports = mongoose.model('Order', orderSchema)
