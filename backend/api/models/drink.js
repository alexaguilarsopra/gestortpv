const mongoose = require('mongoose')

const drinkSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  brand: { type: String, required: true },
  price: { type: Number, required: true },
  stock: { type: Number, required: true },
  consumed: { type: Number, required: true },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: true,
  },
  alcoholic: { type: Boolean, required: true },
  best_of: { type: Boolean, required: true },
  vip: {
    bottle_price: { type: Number },
  },
})

module.exports = mongoose.model('Drink', drinkSchema)
