const mongoose = require('mongoose')

const billingSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  billingType: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'BillinType',
    required: true
  },
  order: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Order',
    required: true
  },
  total: {type: Number}
})

module.exports = mongoose.model('Billing', billingSchema)