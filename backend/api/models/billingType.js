const mongoose = require('mongoose')

const billingTypeSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  type: { type: String, required: true },
})

module.exports = mongoose.model('BillinType', billingTypeSchema)
