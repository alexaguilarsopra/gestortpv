const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const http = require('http')
const cors = require('cors')

const categoriesRoutes = require('./api/routes/categories')
const drinksRoutes = require('./api/routes/drinks')
const orderRoutes = require('./api/routes/orders')
const billingTypeRoutes = require('./api/routes/billingTypes')
const billsRoutes = require('./api/routes/bills')
const user = require('./api/routes/admin')

mongoose.connect(
  'mongodb+srv://alexaguilar:gestortpv@tpv.6uqg7.mongodb.net/gestortpv?retryWrites=true&w=majority',
  { useNewUrlParser: true, useUnifiedTopology: true },
  console.log('Connected to DB')
)

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Request-Width, Content-Type, Accept, Authorization, *' 
  )
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, PUT')
    return res.status(200).json({})
  }
  next()
})

app.use('/categories', categoriesRoutes)
app.use('/drinks', drinksRoutes)
app.use('/orders', orderRoutes)
app.use('/billingTypes', billingTypeRoutes)
app.use('/bills', billsRoutes)
app.use('/user', user)

const port = process.env.PORT || 9000

const server = http.createServer(app)

server.listen(port)

app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message,
    },
  })
})
